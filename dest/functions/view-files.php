<?php 
function add_my_files(){
  global $post;
  $version = '2022042201';
  $DIR = get_template_directory_uri();

  /*
    UIkiti
  /*---------------------------------------*/
  wp_enqueue_style('uikit.css', $DIR . '/plugins/uikit/uikit.min.css',null,'3.13.7','all');
  wp_enqueue_script('uikit.js', $DIR . '/plugins/uikit/uikit.min.js', null, '3.13.7', true);
  wp_enqueue_script('uikit-icons.js',$DIR . '/plugins/uikit/uikit-icons.min.js', null, '3.13.7', true);

  /*
    Vars 
  /*---------------------------------------*/
  wp_localize_script( 'uikit.js', 'api',[
  //   'url' => esc_url_raw( home_url() ),
  //   'root' => esc_url_raw( rest_url() ),
  //   'dir' => esc_url_raw($DIR),
  //   'slug' => esc_html($post->post_name),
  //   'type' => esc_html($post->post_type),
  //   'id' => esc_html($post->ID),
    'nonce' => wp_create_nonce( 'wp_rest' )
  ]);
  wp_enqueue_script('uikit.js');

}
//アクションに追加
add_action('wp_enqueue_scripts','add_my_files');


/**
 * rssやwp_generator等の表記を削除
 *
 **/
function disable_worthless_tags(){
  remove_action('wp_head','rsd_link');
  remove_action('wp_head','wlwmanifest_link');
  remove_action('wp_head','wp_generator');
  remove_action('wp_head','print_emoji_detection_script',7);
  remove_action('admin_print_scripts','print_emoji_detection_script');
  remove_action('wp_print_styles','print_emoji_styles');
  remove_action('admin_print_styles','print_emoji_styles');
  remove_action('wp_head','rest_output_link_wp_head');
  remove_action('wp_head','wp_oembed_add_discovery_links');
  remove_action('wp_head','wp_oembed_add_host_js');
}
add_action('init','disable_worthless_tags');


