<?php

/**
 * サムネイルを利用
 *
 **/
function setup_thumbnail(){
  //- サムネイル
  add_theme_support('post-thumbnails');
  //- カスタムヘッダー
  add_theme_support('custom-header');
}
// add_action('init','setup_thumbnail');




/**
 * カスタムナビゲーションを仕様
 *
 **/
function setup_custom_navigation(){
  //- ナビゲーション
  register_nav_menus([
    'global_navigation'		=>	'global_navigation',
  ]);

  //- ウィジェット
  // register_sidebar([
  // 	'name' => 'お知らせウィジェット',
  // 	'id' => 'sidebar-news',
  // ]);
}
// add_action('after_setup_theme','setup_custom_navigation');



/**
 * カスタム投稿タイプの登録
 *
 **/
function add_custom_post_type(){

  /*
    Shop
  /*---------------------------------------*/
  //ラベル
  $labels = array(
    'name'			=>	'店舗管理',
    'add_new'		=>	'店舗を追加',
    'add_new_item'	=>	'新しい店舗',
    'edit_item'		=>	'店舗を編集',
    'new_item'		=>	'新しい店舗',
    'view_item'		=>	'店舗を見る',
    'not_found'		=>	'店舗はありません',
    'not_found_in_trash'	=>	'ゴミ箱に店舗はありません'
  );
  //パラメーター
  $args = array(
    'labels'		=>	$labels,
    'public'		=>	true,
    'has_archive'	=>	true,
    'show_ui'		=>	true,
    'show_in_rest'	=>	true,
    'supports'		=>	[
      'title','thumbnail',
    ],
    'menu_icon'		=>	'dashicons-megaphone',
    'menu_position'	=>	5
  );
  //登録
  register_post_type('office',$args);

  //タクソノミーパラメーター
  $category_args = array(
    'label'			=>	'店舗カテゴリー',
    'labels'		=>	array(
      'edit_item'		=>	'カテゴリーを編集',
      'add_new_item'	=>	'カテゴリーを追加',
    ),
    'publick'		=>	true,
    'hierarchical'	=> true
  );

  //タクソノミー登録
  register_taxonomy(
    'office-types',
    'office',
    $category_args
  );


}
// add_action('init','add_custom_post_type');


/*
  Custom Post Type Archives 
/*---------------------------------------*/
function post_has_archive( $args, $post_type ) {
  if ( 'post' == $post_type ) {
    $args['rewrite'] = true;
    $args['has_archive'] = 'news'; // ページ名
  }
  return $args;
}
// add_filter( 'register_post_type_args', 'post_has_archive', 10, 2 );
