<?php 

/*
  View 
/*---------------------------------------*/
if(!is_admin()){
  get_template_part('functions/view','files');
  get_template_part('functions/view','common');
  // get_template_part('functions/view','settings');

/*
  Admin 
/*---------------------------------------*/
}else{
  get_template_part('functions/admin','settings');
}

/*
  Common 
/*---------------------------------------*/
get_template_part('functions/common','settings');

/*
  Login 
/*---------------------------------------*/
// if(in_array($GLOBALS['pagenow'],array('wp-login.php','wp-register.php'))){
// 	get_template_part('functions/login','settings');
// }

